# Gitlab CI Kubernetes Google Cloud Boilerplate

A boilerplate to get you started deploying your apps onto GKE kubernetes clusters via Gitlab CI/CD

## Getting Started

Get started by forking this repo and trying to deploy it onto a cluster. It is also misused as testbench for new pipeline configs.

### Prerequisites

What things you need to install the software and how to install them.

[Fork me](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html)

```
A forked version of this repo
```

[Google Cloud SDK](https://cloud.google.com/sdk/)

```
Install Google cloud sdk
```

A kubernetes cluster. [Quickstart guide](https://cloud.google.com/kubernetes-engine/docs/quickstart)

```
Create cluster with 3 nodes, preemptive mirco/small
```

### Installing

Login in gcloud

```
$ gcloud auth login
```

Create/find a service key via [Managing Service account keys](https://cloud.google.com/iam/docs/creating-managing-service-account-keys)

```
Download a JSON service key which we name service_key.json
```

Encode service key for later use as value for env variable `$GCLOUD_SERVICE_KEY`

```
$ cat service_key.json | base64
```

Set the following `Settings > CI/CD` variables

```
GCLOUD_SERVICE_KEY: base64 encoded content of previous step
DOCKER_REGISTERY_IMAGE: i.e. gcr.io/yespleaseai/gitlabtest
CLOUDSDK_CORE_PROJECT: yesplease
CLOUDSDK_CONTAINER_CLUSTER: testcluster
CLOUDSDK_COMPUTE_ZONE: europe-west4
PRODUCTION_DEPLOYMENT_NAME: helloweb
STAGING_DEPLOYMENT_NAME: helloweb
```

## Deployment

Now make a change to the `index.js` and commit to gitlab

## Built With

* Gitlab
* Docker
* Kubernetes
* Google Cloud

## Contributing

Create a merge request

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* @gitlab - You rule!
* @gitlab-examples - Saved me a lot of work 🤘
* @bakkerpeter - For pushing me to this #kubelife
