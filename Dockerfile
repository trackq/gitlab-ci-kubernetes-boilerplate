
FROM node:10-alpine

ENV APP_HOME /app
RUN mkdir -p $APP_HOME
WORKDIR $APP_HOME

COPY package.json yarn.lock ./
RUN yarn

COPY . ./

EXPOSE 3000

CMD ["node", "index.js"]
