const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => res.send('Hello world!'))

app.get('/k8s', (req, res) => res.send('Yeah son! Living that #kubelive'))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
